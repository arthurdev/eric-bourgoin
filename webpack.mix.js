const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.disableNotifications();

if (mix.inProduction()) {
    mix.version();
}

mix.autoload({
    'jquery': ['$', 'window.jQuery']
});

/*
 |--------------------------------------------------------------------------
 | Website
 |--------------------------------------------------------------------------
*/

mix.js('resources/js/app.js', 'public/js')
.sass('resources/sass/app.scss', 'public/css');

mix.sass('resources/sass/index.scss', 'public/css');

mix.sass('resources/sass/login.scss', 'public/css');


/*
 |--------------------------------------------------------------------------
 | Admin
 |--------------------------------------------------------------------------
*/

mix.js('resources/js/admin/app.js', 'public/js/admin')
.sass('resources/sass/admin/app.scss', 'public/css/admin')

mix.js('resources/js/admin/index.js', 'public/js/admin')
