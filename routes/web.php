<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/posts', 'HomeController@findPosts')->name('posts.find');
Route::post('/email/send', 'HomeController@sendEmail')->name('email.send');

Route::group(['prefix' => 'admin'], function() {
    Auth::routes(['register' => false, 'verify' => false, 'reset' => false]);

    Route::namespace('Admin')->middleware('auth')->group(function() {
        Route::get('/', 'HomeController@index')->name('admin.index');
        Route::get('/dt/post/data', 'PostController@datatableData')->name('post.datatable.data');
        Route::resource('/posts', 'PostController')->except(['index', 'show', 'create']);
    });
});
