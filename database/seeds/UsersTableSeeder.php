<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            ['first_name' => 'Arthur','name' => 'Bourgoin','email' => 'arthur.bourgoin@gmail.com','password' => '$2y$10$Rs5cEGuOWU0TZul4/qfXcewwfA0HhC03A7xZLGB5xJBi6JbP8Cwue'],
            ['first_name' => 'Eric','name' => 'Bourgoin','email' => 'eric.bourgoin3@wanadoo.fr','password' => '$2y$12$Ws9flwG6ffkSTqjKG0dbW.pxXv5mKFlBboa4LwjQOMaEb0r6sgNse']
        ]);
    }
}
