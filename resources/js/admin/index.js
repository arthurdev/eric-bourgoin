(function ($) {
    $.indexAdmin = {
        init: function (dtDataUrl) {
            const that = this;

            /* Init DT*/
            that.initDatatables();

            /* Create post*/
            $(".create-post").on("click", function() {
                that.createPost($(this));
            });

            /* Edit post => render modal */
            $("#posts-table").on("click", '.edit-post', function() {
                that.editPost($(this));
            });

            $("#modal-post-update").on("click", ".update-post", function(e) {
                e.preventDefault();
                that.updatePost($(this));
            });

            /* Hide content on hiddenBsModal */
            $("#modal-post-update").on("hidden.bs.modal", function () {
                $(this).find('.modal-form').html('');
                $(this).find('.modal-loader').show();
            });

            /* Delete post */
            $(".table").on("click", '.delete-dt-item', function (e) {
                e.preventDefault();
                that.deleteDtItem($(this));
            });
        },
        createPost: function (elem)
        {
            const url = elem.data('url');
            const btnIcon = elem.find('i');
            const description = $.trim($("#description").val());
            const category = $("#category").val();
            const mainImage = $("#main-image").prop('files');
            const secondImage = $("#second-image").prop('files');

            let formData = new FormData();

            formData.append('description', description);
            formData.append('category', category);

            if (mainImage.length) {
                formData.append('main_image', mainImage[0])
            }

            if (secondImage.length) {
                formData.append('second_image', secondImage[0])
            }

            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: formData,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    elem.prop('disabled', true);
                    btnIcon.removeClass('fa-save');
                    btnIcon.addClass('fa-spinner fa-spin');
                }
            })
            .done(function(response) {
                if (response.status) {
                    toastr.success(response.message)

                    $("#modal-post-create").modal('hide');
                    $("#posts-table").DataTable().ajax.reload();
                }
            })
            .fail(function(err) {
                let errors = err.responseJSON.errors
                toastr.error(Object.values(errors)[0][0])
            })
            .always(function() {
                $("#modal-post-create .modal-body form").trigger('reset');
                elem.prop('disabled', false);
                btnIcon.removeClass('fa-spinner fa-spin');
                btnIcon.addClass('fa-save');
            })
        },
        editPost: function (elem) {
            const url = elem.attr('href');
            const modalLoader = $("#modal-post-update .loader");
            const modalForm = $("#modal-post-update .modal-form");

            $.get(url, function(response) {
                modalLoader.hide();
                modalForm.html(response.html);
            })
        },
        updatePost: function (elem) {
            const url = elem.data('url');
            const btnIcon = elem.find('i');

            const description = $.trim($("#update-description").val());
            const category = $("#update-category").val();
            const mainImage = $("#update-main-image").prop('files');
            const secondImage = $("#update-second-image").prop('files');

            let formData = new FormData();

            formData.append('description', description);
            formData.append('category', category);

            if (mainImage.length) {
                formData.append('main_image', mainImage[0])
            }

            if (secondImage.length) {
                formData.append('second_image', secondImage[0])
            }

            formData.append('_method', 'PUT');

            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: formData,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    elem.prop('disabled', true)
                    btnIcon.removeClass('fas fa-save')
                    btnIcon.addClass('fas fa-spinner fa-spin')
                }
            })
            .done(function(response) {
                if (response.status) {
                    toastr.success(response.message)

                    $("#modal-post-update").modal('hide');
                    $("#posts-table").DataTable().ajax.reload();
                }
            })
            .fail(function(err) {
                let errors = err.responseJSON.errors
                toastr.error(Object.values(errors)[0][0])
            })
            .always(function() {
                elem.prop('disabled', false)
                btnIcon.removeClass('fas fa-spinner fa-spin')
                btnIcon.addClass('fas fa-save')
            });
        },
        deleteDtItem: function (elem) {
            const url = elem.attr('href');
            const icon = elem.find('i');
            const csrfToken = $("meta[name='csrf-token']").attr("content");

            swal({
                title: "Êtes-vous sûr de vouloir supprimer cette publication ?",
                text: "Une fois supprimée, la publication ne pourra être récupérée",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: url,
                        type: 'DELETE',
                        dataType: 'json',
                        data: {
                            _token: csrfToken
                        },
                        beforeSend: function () {
                            elem.prop('disabled', true);
                            icon.removeClass('fa-trash');
                            icon.addClass('fa-spinner fa-spin');
                        }
                    })
                    .done(function(data) {
                        if (data.status) {
                            swal("Publication supprimée !", {icon: "success"});
                            $("#posts-table").DataTable().ajax.reload();
                        }
                    })
                    .fail(function(err) {})
                    .always(function() {
                        elem.prop('disabled', false);
                        icon.removeClass('fa-spinner fa-spin');
                        icon.addClass('fa-trash');
                    });
                } else {
                    swal("Supression annulée !");
                }
            });
        },
        initDatatables() {
            $("#posts-table").DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: dtDataUrl,
                order: [3, 'desc'],
                columns: [
                    {
                        data: 'image',
                        title: 'Image',
                        width: '7%',
                        render: function( data ) {
                            return "<img src=\"/storage/" + data + "\" style=\"max-width:100%;\"/>";
                        }
                    },
                    {
                        data: 'description',
                        title: 'Description',
                        width: '15%'
                    },
                    {
                        data: 'category',
                        title: 'Catégorie',
                        width: '5%'
                    },
                    {
                        data: 'created_at',
                        title: 'Créé le',
                        width: '5%'
                    },
                    {
                        data: 'actions',
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        width: '10%'
                    }
                ],
                language: {
                    url: '/plugins/datatable/dt-language-fr.json'
                }
            });
        }
    }
})(jQuery);
