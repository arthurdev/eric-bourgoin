/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./../bootstrap');

import DataTable from 'datatables.net'
import 'datatables.net-bs4/js/dataTables.bootstrap4.js'
import 'datatables.net-responsive/js/dataTables.responsive.min.js'
import 'datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js'
import 'datatables.net-bs4/css/dataTables.bootstrap4.css'
import 'datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css'

import swal from 'sweetalert'
import toastr from 'toastr'

window.toastr = toastr


