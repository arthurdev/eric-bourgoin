<!DOCTYPE html>
<html lang="fr">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-59671380-4"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-59671380-4');
    </script>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="Artisan ébéniste et menuisier, restauration de meubles anciens, fabrication et aménagements de cuisines">

    <title>{{ config('app.name', 'Eric Bourgoin') }}</title>

    <link rel="icon" href="{{ asset('images/favicon.png') }}" />
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/index.css') }}" rel="stylesheet">
</head>

<body>
    <main class="flex-grow-1">
        <nav class="navbar fixed-top navbar-expand-lg navbar-light py-2" id="navbar-custom">
            <div class="container">
                <div class="d-flex align-items-center d-lg-block w-100">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#nav-toggler"
                    aria-controls="nav-toggler" aria-expanded="false" aria-label="Toggle navigation">
                        <span><i class="fas fa-bars"></i></span>
                    </button>
                    <div class="collapse navbar-collapse" id="nav-toggler">
                        <ul class="navbar-nav flex-row justify-content-around w-100">
                            <li class="nav-item d-flex align-items-center">
                                <a class="nav-link scroll-to" href="#about">
                                    <i class="fas fa-address-card"></i>
                                    <span class="d-none d-md-inline">A propos</span>
                                </a>
                            </li>
                            <li class="nav-item d-flex align-items-center">
                                <a class="nav-link scroll-to" href="#work">
                                    <i class="fas fa-tools"></i>
                                    <span class="d-none d-md-inline">Réalisations</span>
                                </a>
                            </li>
                            <li class="nav-item d-flex align-items-center">
                                <a class="navbar-brand scroll-to" href="#header">
                                    <img src="{{ asset('images/logo.png') }}" class="navbar-logo" alt="logo">
                                </a>
                            </li>
                            <li class="nav-item d-flex align-items-center">
                                <a class="nav-link scroll-to" href="#contact">
                                    <i class="far fa-address-book"></i>
                                    <span class="d-none d-md-inline">Contact</span>
                                </a>
                            </li>
                            <li class="nav-item d-flex align-items-center">
                                <a class="nav-link scroll-to" href="#findme">
                                    <i class="fas fa-map-marker-alt"></i>
                                    <span class="d-none d-md-inline">Coordonnées</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>

        <header class="page-header" id="header">
            <div class="container h-100">
                <div class="row h-100 justify-content-center align-items-center text-center">
                    <div class="col-lg-10 align-self-end">
                        <h1 class="text-uppercase text-white font-weight-bold">
                            Eric bourgoin
                        </h1>
                        <hr class="bg-primary my-4 divider">
                    </div>
                    <div class="col-lg-8 align-self-baseline">
                        <h3 class="text-white mb-5">Artisan ébéniste - menuisier</h3>

                        <button class="btn btn-primary btn-lg rounded-pill text-white px-4 py-2 scroll-to" href="#about">
                            <i class="fas fa-arrow-down"></i>
                            En savoir plus
                        </button>
                    </div>
                </div>
            </div>
        </header>
        
        <section class="container page-section" id="about">
            <h2 class="text-center text-uppercase">A propos de moi</h2>
            <hr class="bg-primary my-4 divider">
            <div class="row">
                <div class="col-lg-4 col-sm-6 col-12 mb-5 mb-lg-0">
                    <div class="text-center about-card h-100">
                        <i class="fas fa-graduation-cap text-primary fa-4x mb-4"></i>
                        <h3 class="mb-3">Ebéniste diplômé</h3>

                        <ul class="list-unstyled mb-0 font-weight-light">
                            <li class="pb-4">BEP <strong>Bois et Matériaux Associés</strong> - 1988</li>
                            <li class="pb-4">1er prix régional de la <strong>Société d'Encouragement aux Métiers d'Art</strong> (SEMA) - 1988</li>
                            <li class="pb-4"><strong>Brevet de Technicien</strong> en agencement - 1990</li>
                            <li><strong>Diplôme des Métiers d'Art</strong> en ébénisterie à <strong>l'école Boulle</strong> - 1993</li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-4 col-sm-6 col-12 mb-5 mb-lg-0">
                    <div class="text-center about-card h-100">
                        <i class="fas fa-tools text-primary fa-4x mb-4"></i>
                        <h3 class="mb-3">Ebéniste expérimenté</h3>

                        <ul class="list-unstyled mb-0 font-weight-light">
                            <li class="pb-4">Salarié à Sens pendant <b>près de 10 ans</b></li>
                            <li class="pb-4">Création de <b>ma propre entreprise artisanale</b> en 2003</li>
                            <li class="pb-4">Plus de <b>1700</b> projets menés à bien</li>
                            <li>Plus de <b>150</b> clients satisfaits</li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-4 col-12 mx-sm-auto">
                    <div class="text-center about-card h-100">
                        <i class="fas fa-users text-primary fa-4x mb-4"></i>
                        <h3 class="mb-3">A votre écoute</h3>

                        <ul class="list-unstyled mb-0 font-weight-light">
                            <li class="pb-4">Elaboration de vos futurs projets</li>
                            <li class="pb-4">Prise en compte de <strong>vos attentes et besoins</strong></li>
                            <li class="pb-4">Matériaux de <strong>qualité</strong> et respect du <strong>travail soigné</strong></li>
                            <li>Pleine <strong>satisfaction des clients</strong></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <section class="page-section" id="work"></section>

        <section class="page-section container" id="contact">
            <h2 class="text-center text-uppercase">Me contacter</h2>
            <hr class="bg-primary my-4 divider">

            <form action="{{ route('email.send') }}">
                <div class="row">
                    <div class="col-12 col-lg-6">
                        <div class="form-group">
                            <label for="name">Nom et prénom *</label>
                            <input class="form-control" type="text" name="name" placeholder="Nom et prénom" id="name">
                        </div>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="form-group">
                            <label for="email">Adresse email *</label>
                            <input class="form-control" type="email" name="email" placeholder="Adresse email" id="email">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="subject">Sujet</label>
                    <input class="form-control" type="text" name="subject" placeholder="Sujet" id="subject">
                </div>
                <div class="form-group">
                    <label for="message">Message</label>
                    <textarea class="form-control" type="text" name="message" placeholder="Message" id="message" rows="4"></textarea>
                </div>

                <button class="btn btn-primary btn-flat text-white rounded-0" type="submit">
                    <i class="fas fa-check"></i>
                    Envoyer
                </button>
            </form>
        </section>

        <section class="page-section" id="findme">
            <h2 class="text-center text-uppercase">Coordonnées</h2>
            <hr class="bg-primary my-4 divider">

            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d57205.02584430666!2d3.4015555387233603!3d48.00999529977381!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47efaad434ee244b%3A0xac764e883e8a8852!2sBourgoin%20Eric!5e0!3m2!1sfr!2sfr!4v1583316869235!5m2!1sfr!2sfr" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="false"></iframe>
        </section>

        <section class="page-section">
            <div class="instagram-join mb-3">
                <h2 class="text-center text-uppercase mb-0 instagram-join-title">Rejoignez-moi sur instagram</h2>
                <img class="instagram-logo ml-sm-2" src="{{ asset('images/instagram-logo.png') }}" alt="Logo instagram">
            </div>
            <div class="text-center instagram-profile">
                <img src="{{ asset('images/instagram_profile.jpg') }}" class="rounded-circle avatar" alt="Photo instagram">
                <a href="https://www.instagram.com/bourgoin_eric_ebeniste/" target="_blank" class="ml-2 username">bourgoin_eric_ebeniste</a>
            </div>

            <hr class="bg-primary my-4 divider">
            
            <div class="owl-carousel owl-theme">
                @foreach ($instagramPosts as $post)
                    <a href="{{ $post['permalink'] }}" class="card d-flex flex-column h-100 instagram-post" target="_blank">
                        <div class="w-100 image" style="background-image:url({{ $post['media_url'] }})"></div>
                        
                        <div class="card-body">
                            <p class="card-text content">{{ $post['caption'] }}</p>
                        </div>
                    </a>
                @endforeach
            </div>
        </section>
    </main>

    <footer class="bg-white w-100 footer font-weight-light">
        <div class="container py-3">
            <div class="row">
                <div class="col d-flex justify-content-center">
                    <ul class="list-unstyled mb-0">
                        <li class="mb-2">
                            <i class="fas fa-home mr-1"></i>
                            5 Grande Rue, 89300 Looze
                        </li>
                        <li class="mb-2">
                            <a href="tel:+33386623263">
                                <i class="fas fa-phone mr-1"></i>
                                03.86.62.32.63
                            </a>
                        </li>
                        <li>
                            <a href="mailto:eric.bourgoin3@wanadoo.fr">
                                <i class="fas fa-envelope mr-1"></i>
                                eric.bourgoin3@wanadoo.fr
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="col d-flex justify-content-center">
                    <ul class="list-unstyled mb-0">
                        <li class="mb-2">SIREN : 444 391 379</li>
                        <li class="mb-2">SIRET : 444 391 379 00013</li>
                        <li>CODE APE : 4332A</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="bg-light text-center py-2">
            <span>
                © {{ date('Y')}} Copyright <strong>Eric Bourgoin </strong>
            </span>
        </div>
    </footer>

    <script src="{{ asset('js/app.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.owl-carousel').owlCarousel({
                loop: true,
                center: true,
                margin: 10,
                nav: true,
                navText : ["<i class='fas fa-chevron-left'></i>","<i class='fas fa-chevron-right'></i>"],
                responsive: {
                    0: {
                        items: 2
                    },
                    600: {
                        items: 3
                    },
                    1000: {
                        items: 5
                    }
                }
            });

            /* Load posts */
            const workContainer = $("#work")

            $.ajax({
                url: '/posts',
                method: 'GET',
                dataType: 'json'
            })
            .done(function(data){
                workContainer.html(data.html)
            });

            /* ScrollTo */
            let isWindowScrolling = false
            const animationDuration = 800
            const navbarHeight = $("#navbar-custom").height();

            $(".scroll-to").on("click", function(e) {
                e.preventDefault();
                
                /* Check if another animation is running */ 
                if (isWindowScrolling) {
                    return false;
                }

                isWindowScrolling = true

                const targetId = $(this).attr('href');
            
                $('html, body').animate({
                    /* Scroll to elem minus the navbar height due to the fixed position */
                    scrollTop: $(targetId).offset().top - navbarHeight - 30
                }, animationDuration);

                setTimeout(function() {
                    isWindowScrolling = false;
                }, animationDuration);
            });

            /* ScrollSpy */
            $('body').scrollspy({target: "#navbar-custom", offset:navbarHeight * 2});

            /* Custom affix */
            $(window).on('load scroll', function(event) {
                const navbar = $('#navbar-custom');
                const scrollValue = $(window).scrollTop();
                const scrollTriggerValue = (navbar.height());

                if (scrollValue >= scrollTriggerValue) {
                    navbar.addClass('affix');
                } else {
                    navbar.removeClass('affix');
                }
            });

            $("#contact form").on("submit", function(event) {
                event.preventDefault();

                const url = $(this).attr('action');
                const fields = $(this).find('input, textarea, button');
                const button = $(this).find('button[type="submit"]');
                const buttonIcon = button.find('i');

                $.ajax({
                    url: url,
                    method: 'POST',
                    dataType: 'json',
                    data: $(this).serialize(),
                    beforeSend: function () {
                        fields.prop('disabled', true)
                        buttonIcon.removeClass('fa-check')
                        buttonIcon.addClass('fa-spinner fa-spin')
                    }
                })
                .done(function(res) {
                    if (res.status) {
                        swal({
                            text: 'Merci ! Je reviens vers vous sous peu !',
                            icon: 'success',
                        });
                    }
                })
                .fail(function(err) {})
                .always(function() {
                    fields.prop('disabled', false)

                    buttonIcon.removeClass('fa-spinner fa-spin')
                    buttonIcon.addClass('fa-check')
                });
            })
        });
    </script>
</body>
</html>
