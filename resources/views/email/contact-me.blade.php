@component('mail::message')
Bonjour, 

Vous avez été contacté par **{{ $name }} ({{ $email }})** depuis votre site web au sujet de ***{{ $subject }}*** : 

*" {{ $message }} "*

Cordialement.
@endcomponent