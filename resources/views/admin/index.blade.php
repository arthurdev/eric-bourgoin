<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Eric Bourgoin') }}</title>
    
    <link href="{{ asset('images/favicon.png') }}" rel="icon"/>
    <link href="{{ asset('css/admin/app.css') }}" rel="stylesheet">
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01"
            aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
            <a class="navbar-brand" href="#">Espace admin</a>

            <div class="ml-auto">
                <a href="{{ route('logout') }}" class="text-danger" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">
                    Déconnexion
                </a>    
            </div>

            <form id="frm-logout" class="d-none" action="{{ route('logout') }}" method="POST">
                {{ csrf_field() }}
            </form>
        </div>
    </nav>

    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                Liste des publications
            </div>
            <div class="card-body">
                <button class="btn btn-success btn-sm mb-2" data-toggle="modal" data-target="#modal-post-create">
                    <i class="fas fa-plus"></i>
                    Ajouter une publication
                </button>
                <table class="table table-striped" id="posts-table">
                    <b-thead>
                        <tr>
                            <th scope="col">Image</th>
                            <th scope="col">Description</th>
                            <th scope="col">Categorie</th>
                            <th scope="col">Date de création</th>
                            <th scope="col">Action</th>
                        </tr>
                    </b-thead>
                </table>
            </div>
        </div>
    </div>

    @include('admin.post.modal-create')
    
    <div class="modal fade" id="modal-post-update" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modifier une publication</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="loader text-center"><i class="fas fa-spinner fa-spin fa-2x"></i></div>

                <section class="modal-form"><!-- Rendered form --></section>
            </div>
        </div>
    </div>

    <script src="{{ asset('js/admin/app.js') }}"></script>
    <script src="{{ asset('js/admin/index.js') }}" defer></script>

    <script>
        let dtDataUrl = "{{ route('post.datatable.data') }}"

        $(document).ready(function () {
            $.indexAdmin.init(dtDataUrl);
        });
    </script>
</body>
