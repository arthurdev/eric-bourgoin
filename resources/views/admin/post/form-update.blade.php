<div class="modal-body">
    <form class="form-update-post">
        <div class="form-group">
            <label for="update-category" class="col-form-label">Catégorie</label>
            <select class="form-control" id="update-category">
                @foreach ($postCategories as $slug => $name)
                    <option value="{{ $slug }}" {{ $slug === $post->category ? 'selected' : null }}>{{ $name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="update-description" class="col-form-label">Description</label>
            <textarea class="form-control" id="update-description" rows="3" placeholder="Description">{{ $post->description }}</textarea>
        </div>
        
        <div class="form-group">
            <label for="update-main-image">Image</label>
            <input type="file" class="form-control-file" id="update-main-image" accept="image/*">
        </div>

        <div class="form-group">
            <label for="update-second-image">Image complémentaire (optionnel)</label>
            <input type="file" class="form-control-file" id="update-second-image" accept="image/*">
        </div>
    </form>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">
        <i class="fas fa-times"></i>
        Fermer
    </button>
    <button type="button" class="btn btn-success btn-sm update-post" data-url="{{ route('posts.update', $post->id) }}">
        <i class="fas fa-save"></i>
        Enregistrer
    </button>
</div>