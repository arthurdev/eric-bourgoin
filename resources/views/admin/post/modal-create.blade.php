<div class="modal fade" id="modal-post-create" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Créer une publication</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="category" class="col-form-label">Catégorie</label>
                        <select class="form-control" id="category">
                            @foreach ($postCategories as $slug => $name)
                                <option value="{{ $slug }}">{{ $name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="description" class="col-form-label">Description</label>
                        <textarea class="form-control" id="description" rows="3" placeholder="Description"></textarea>
                    </div>
                    
                    <div class="form-group">
                        <label for="main-image">Image</label>
                        <input type="file" class="form-control-file" id="main-image" accept="image/*">
                    </div>

                    <div class="form-group">
                        <label for="second-image">Image complémentaire (optionnel)</label>
                        <input type="file" class="form-control-file" id="second-image" accept="image/*">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">
                    <i class="fas fa-times"></i>
                    Fermer
                </button>
                <button type="button" class="btn btn-success btn-sm create-post" data-url="{{ route('posts.store') }}">
                    <i class="fas fa-save"></i>
                    Enregistrer
                </button>
            </div>
        </div>
    </div>
</div>
