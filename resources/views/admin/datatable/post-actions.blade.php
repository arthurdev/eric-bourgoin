<a href="{{ route('posts.edit', $id) }}" class="btn btn-info btn-sm edit-post" role="button" data-toggle="modal" data-target="#modal-post-update">
    <i class="fas fa-edit"></i>
    Editer
</a>

<a href="{{ route('posts.destroy', $id) }}" class="btn btn-danger btn-sm delete-dt-item" role="button">
    <i class="fas fa-trash"></i>
    Supprimer
</a>