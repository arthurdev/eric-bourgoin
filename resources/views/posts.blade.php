<div class="container">
    <h2 class="text-center text-uppercase">Mes réalisations</h2>
    <hr class="bg-primary my-4 divider">

    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
        @foreach ($postCategories as $slug => $name)
            <li class="nav-item">
                <a class="nav-link {{ $loop->first ? 'active' : null }}" data-toggle="pill" href="{{ '#' . $slug }}" role="tab">{{ $name }}</a>
            </li>
        @endforeach
    </ul>
</div>

<div class="px-1">
    <div class="tab-content">
        @foreach ($postCategories as $slug => $name)
            <div class="tab-pane fade {{ $loop->first ? 'show active' : null }}" id="{{ $slug }}" role="tabpanel">
                <div class="card-columns">
                    @foreach ($posts[$slug] as $post)
                        <div class="card post">
                            <img src="{{ asset('storage/' . $post->image) }}" class="img-fluid"/>
                            <div class="overlay d-flex justify-content-center align-items-center">
                                <p class="description text-white text-center m-0">{{ $post->description }}</p>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @endforeach
    </div>
</div>