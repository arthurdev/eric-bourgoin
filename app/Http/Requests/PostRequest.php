<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Enum\PostCategoryEnum;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category' => Rule::in(PostCategoryEnum::getSlugs()),
            'main_image' => $this->route('post') ? 'required_with:second_image' : 'required' . '|image|mimes:jpeg,png,jpg,svg,webp|max:2048',
            'second_image' => 'sometimes|image|mimes:jpeg,png,jpg,svg,webp|max:2048',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'category' => 'Catégorie',
            'main_image' => 'Image principale',
            'second_image' => 'Image secondaire',
        ];
    }
}
