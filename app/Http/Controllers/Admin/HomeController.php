<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Enum\PostCategoryEnum;

class HomeController extends Controller
{
    /**
     * Admin index
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.index', array(
            'postCategories' => PostCategoryEnum::getAll()
        ));
    }
}
