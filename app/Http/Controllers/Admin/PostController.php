<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostRequest;
use App\Post;
use DataTables;
use Image;
use Illuminate\Support\Facades\Storage;
use App\Enum\PostCategoryEnum;
use Illuminate\Http\UploadedFile;

class PostController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        // File upload & merge if second image is present
        $imagePath = $this->handleImageUpload($request->file('main_image'), $request->file('second_image'));

        $post = new Post();

        $post->description = $request->description;
        $post->category = $request->category;
        $post->image = $imagePath;

        $post->save();

        return response()->json(array(
            'status' => true,
            'message' => 'Publication enregistrée'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        return response()->json(array(
            'html' => view('admin.post.form-update', 
                array('post' => $post, 'postCategories' => PostCategoryEnum::getAll())
            )->render()
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, Post $post)
    {
        if ($request->hasFile('main_image')) {
            // Upload file
            $imagePath = $request->file('main_image')->store('posts', 'public');

            $post->image = $imagePath;
        }

        if ($request->has('description')) {
            $post->description = $request->description;
        }

        if ($request->has('category')) {
            $post->category = $request->category;
        }

        $post->save();

        return response()->json(array(
            'status' => true,
            'message' => 'Publication modifiée'
        ));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete();

        return response()->json([
            'status' => true,
            'message' => 'Publication supprimée'
        ]);
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function datatableData()
    {
        return Datatables::of(Post::all())
            ->addColumn('actions', "admin.datatable.post-actions")
            ->rawColumns(['actions'])
            ->editColumn('created_at', function ($request) {
                return $request->created_at->format('d/m/Y H:i');
            })
            ->editColumn('category', function ($request) {
                return PostCategoryEnum::getName($request->category);
            })
            ->make(true);
    }

    private function handleImageUpload(UploadedFile $mainImage, ?UploadedFile $secondImage) 
    {
        // If only main image
        if (!$secondImage) {
            return $mainImage->store('posts', 'public');
        }

        // Else => create new image from the others
        $mainImageObject = Image::make($mainImage);
        $secondImageObject = Image::make($secondImage);

        // Reduce height of the highest image and find new image height
        if ($mainImageObject->height() < $secondImageObject->height()) {
            $newImageHeight = $mainImageObject->height();
            $secondImageObject->heighten($mainImageObject->height());
        } else {
            $newImageHeight = $secondImageObject->height();
            $mainImageObject->heighten($secondImageObject->height());
        }

        $newImageWidth = $mainImageObject->width() + $secondImageObject->width();

        $newImage = Image::canvas($newImageWidth, $newImageHeight);
        $newImage->insert($mainImageObject);
        $newImage->insert($secondImageObject, 'right');
        $newImage->encode('jpg');

        $hash = md5($newImage->__toString());
        $filePath = 'posts/' . $hash . '.jpg';

        Storage::disk('public')->put($filePath, $newImage);

        return $filePath;
    }
}
