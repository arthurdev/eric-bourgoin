<?php

namespace App\Http\Controllers;

use App\Post;
use App\InstagramToken;
use App\Mail\ContactMe;
use Illuminate\View\View;
use Illuminate\Http\Request;
use App\Enum\PostCategoryEnum;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\ContactRequest;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return View
     */
    public function index(): View
    {
        $instagramFields = array('caption', 'id', 'media_type', 'media_url', 'permalink', 'thumbnail_url', 'timestamp', 'username');

        $instagramTokenObject = InstagramToken::first();

        if ($instagramTokenObject) {
            $response = Http::get(config('instagram.base_url') . '/me/media', array(
                'access_token' => $instagramTokenObject->token,
                'fields' => implode(',', $instagramFields)
            ));
    
            $instagramPosts = $response->successful() ? $response->json()['data'] : array();
        } else {
            $instagramPosts = array();
        }

        return view('index', array(
            'instagramPosts' => $instagramPosts
        ));
    }

    /**
     * Load the posts 
    */
    public function findPosts(Request $request): JsonResponse
    {
        if (!$request->ajax()) {
            abort(404);
        }

        $posts = Post::all()->shuffle();

        $postsByCategory = array();

        foreach (PostCategoryEnum::getSlugs() as $categorySlug) {
            $postsByCategory[$categorySlug] = $posts->filter(function($post) use ($categorySlug) {
                return $post->category === $categorySlug;
            });
        }

        return response()->json([
            'status' => true,
            'html' => view('posts', array(
                'postCategories' => PostCategoryEnum::getAll(),
                'posts' => $postsByCategory
            ))->render() 
        ]);
    }

    /**
     * Send email from contact form
     * @param  ContactRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function sendEmail(ContactRequest $request)
    {
        Mail::to(env('MAIL_TO_CONTACT'))->send(new ContactMe($request->request->all()));

        return response()->json(array(
            'status' => true,
            'message' => 'Email envoyé'
        ));
    }
}
