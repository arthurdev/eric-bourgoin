<?php

namespace App\Console\Commands;

use App\InstagramToken;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class RefreshInstagramApiToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'instagram:token:refresh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refresh and save instagram API token';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $tokenObject = InstagramToken::first();

        $url = config('instagram.base_url') . '/refresh_access_token';
        
        $response = Http::get($url, array(
            'grant_type' => 'ig_refresh_token',
            'access_token' => $tokenObject->token
        ));

        if ($response->successful()) {
            $tokenObject->token = $response->json()['access_token'];

            $tokenObject->save();

            return $this->info('API token refreshed !');
        }

        return $this->error('Errors in API response');
    }
}
