<?php

namespace App\Enum;

abstract class PostCategoryEnum
{
    const RESTAURATION  = "restauration";
    const FABRICATION   = "fabrication";
    const CUISINE      = "cuisine";
    const INTERIEUR     = "interieur";

    /** @var array user friendly named etat */
    protected static $categoryName = [
        self::RESTAURATION  => 'Restauration de meubles anciens',
        self::FABRICATION   => 'Fabrication sur-mesure',
        self::CUISINE      => 'Aménagement de cuisines',
        self::INTERIEUR     => 'Aménagement intérieur'
    ];

    /**
     * @param  string $categorySlug
     * @return string
     */
    public static function getName(string $categorySlug): string
    {
        if (!isset(static::$categoryName[$categorySlug])) {
            return "Catégorie ".__CLASS__." inconnu ($categorySlug)";
        }

        return static::$categoryName[$categorySlug];
    }

    /**
     * @param  string $categorySlug
     * @return boolean
     */
    public static function checkSlug(string $categorySlug): string
    {
        if (!isset(static::$categoryName[$categorySlug])) {
            return false;
        }

        return true;
    }

    /**
     * @return array
     */
    public static function getSlugs(): array
    {
        return [
            self::RESTAURATION,
            self::FABRICATION,
            self::CUISINE,
            self::INTERIEUR
        ];
    }

    /**
     * @return array
     */
    public static function getAll(): array
    {
        return self::$categoryName;
    }
}
